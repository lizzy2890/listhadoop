package Test.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void listFilesDNames(FileSystem fs, FileStatus[] status,int nivel) throws IOException{
		for(int i=0;i<status.length;i++){
		if(!status[i].isDir()){
			for(int j=0;j<nivel;j++){
				System.out.print("\t");
			}
			System.out.println(status[i].getPath().getName());
        }
        else{
		for(int j=0;j<nivel;j++){
			System.out.print("\t");
		}
        	System.out.println(status[i].getPath());
        	FileStatus[] status1 = fs.listStatus(status[i].getPath());
        	listFilesDNames(fs,status1,nivel+1);
        	}
		}
		return;
	}
	
	public static void main (String [] args) throws Exception{
		 Configuration conf = new Configuration();
		 conf.addResource(new Path("/usr/local/hadoop/etc/hadoop/core-site.xml"));
		
		try{
		FileSystem fs = FileSystem.get(conf);
                FileStatus[] status = fs.listStatus(new Path("hdfs://localhost:9000/vero"));
		System.out.println("hdfs://localhost:9000/vero");
		listFilesDNames(fs, status,0);
                
        }catch(Exception e){
        		System.err.println(e);
                System.out.println("File not found");
        }
}

}

